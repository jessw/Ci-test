import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestBasicClass {
    private BasicClass basicClass;
    @Before
    public void setup() {
      basicClass = new BasicClass();
    }

    @Test
    public void testAddToMap() {
        basicClass.addToMap("test", 3);
        assertEquals(basicClass.getTestMap().size(), 1);
        assertEquals(basicClass.getTestMap().get("test"), new Integer(3));
    }

    @Test
    public void testRemoveFromMap() {
        basicClass.addToMap("test1", 3);
        basicClass.addToMap("test2", 4);
        basicClass.addToMap("test3", 9);
        assertEquals(basicClass.getTestMap().size(), 3);
        basicClass.removeFromMap("test2");
        assertEquals(basicClass.getTestMap().size(), 2);
    }

    @Test
    public void testGetTestInt() {
        assertEquals(basicClass.getTestInt(), 2);
    }

    @Test
    public void testSetTestInt() {
        basicClass.setTestInt(5);
        assertEquals(basicClass.getTestInt(), 5);
    }

    @Test
    public void getTestString() {
        assertEquals(basicClass.getTestString(), "Hey wuddup");
    }

    @Test
    public void testSetTestString() {
        basicClass.setTestString("tj cups");
        assertEquals(basicClass.getTestString(), "tj cups");
    }
}
