import java.util.HashMap;
import java.util.Map;

public class BasicClass {
    private int testInt = 2;
    private String testString = "Hey wuddup";
    private Map<String, Integer> testMap;

    public BasicClass() {
        testMap = new HashMap<String, Integer>();
    }

    public void addToMap(String key, int value) {
        testMap.put(key, value);
    }

    public void removeFromMap(String key) {
        testMap.remove(key);
    }

    public int getTestInt() {
        return testInt;
    }

    public void setTestInt(int testInt) {
        this.testInt = testInt;
    }

    public String getTestString() {
        return testString;
    }

    public void setTestString(String testString) {
        this.testString = testString;
    }

    public Map<String, Integer> getTestMap() {
        return testMap;
    }

}
